import java.util.Scanner;

public class Aufgabe2Steuersatz {

	public static void main(String[] args) {
		Scanner tastatur = new Scanner(System.in);
		System.out.println("Geben Sie den Nettowert an:");
		double nettopreis = tastatur.nextDouble();

		System.out.println(
				"Entscheiden Sie sich mit j f�r den erm��igten Steuersatz, oder mit n f�r den vollen Steuersatz.");

		String entscheidung = tastatur.next();

		if (entscheidung.equalsIgnoreCase("j")) {

			double bruttosatzerm��igt = nettopreis + (nettopreis * 0.07);

			System.out.println("Der korrekte Bruttobetrag ist: ");

			System.out.printf("%.2f", bruttosatzerm��igt);

			System.out.println(" Euro.");
		}

		else if (entscheidung.equalsIgnoreCase("n")) {
			double bruttosatz = nettopreis + (nettopreis * 0.19);

			System.out.println("Der korrekte Bruttobetrag ist: ");

			System.out.printf("%.2f", bruttosatz);

			System.out.println(" Euro.");
			
		} else {

			System.out.println("Die Eingabe ist ung�ltig");
		}
	}
}
