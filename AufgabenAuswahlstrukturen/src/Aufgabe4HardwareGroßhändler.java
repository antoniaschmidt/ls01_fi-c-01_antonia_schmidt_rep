import java.util.Scanner;

public class Aufgabe4HardwareGro�h�ndler {

	public static void main(String[] args) {

		Scanner tastatur = new Scanner(System.in);

		System.out.println("Geben Sie die Anzahl der bestellten M�use ein: ");
		int bestellteM�use = tastatur.nextInt();
		System.out.println("Geben Sie den Nettopreis einer Maus ein: ");
		double preisMaus = tastatur.nextDouble();

		if (bestellteM�use >= 10) {

			double preis = ((preisMaus * 0.19) + preisMaus) * 10;

			System.out.println("Der Preis, ohne Lieferpauschale, betr�gt: ");
			System.out.printf("%.2f", preis);
			System.out.println(" Euro.");

		} else if (bestellteM�use < 10) {
			double preis = (((preisMaus * 0.19) + preisMaus) * 10) + 10;

			System.out.println("Der Preis, mit 10 Euro Lieferpauschale, betr�gt: ");
			System.out.printf("%.2f", preis);
			System.out.println(" Euro.");

		} else {

			System.out.println("Die Angaben sind ung�ltig.");

		}
	}

}
