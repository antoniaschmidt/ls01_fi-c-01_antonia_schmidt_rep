import java.util.Scanner;

public class Mathe {

	public static double quadrat(double x) {
		
		double ergebnis;
		ergebnis= x*x;
		return ergebnis;
		
	}
	public static void main(String[] args) {
		
		Scanner meinScanner= new Scanner(System.in);
		
		double zahl1;
		double quadratzahl;
		
		System.out.print("Wie lautet die Zahl, die quadriert werden soll: ");
		zahl1=meinScanner.nextDouble();
		
		quadratzahl=quadrat(zahl1);
		
		System.out.print("Das Quadrat von "+zahl1+" ist "+quadratzahl);
		
	}

}
