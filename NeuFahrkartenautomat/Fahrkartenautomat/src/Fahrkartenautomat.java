﻿import java.util.Scanner;

class Fahrkartenautomat {
	public static double fahrkartenbestellungErfassen() {
		double preis=0;
		double gesamtbetrag;
		System.out.println("Fahrkartenbestellvorgang:");
		for (int i = 0; i < 12; i++) {
			System.out.print("=");
			try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("\n\n");

	
		// Vorgang
		Scanner tastatur = new Scanner(System.in);

		// Eingaben
		double [] preise=new double[10];
		preise[0]=2.90;
		preise[1]=3.30;
		preise[2]=3.60;		
		preise[3]=1.90;
		preise[4]=8.60;
		preise[5]=9.00;
		preise[6]=9.60;
		preise[7]=23.50;
		preise[8]=24.30;
		preise[9]=24.90;
		
		String[] fahrkarten= new String[10];
		fahrkarten[0]="Einzelfahrschein Berlin AB";
		fahrkarten[1]="Einzelfahrschein Berlin BC";
		fahrkarten[2]="Einzelfahrschein Berlin ABC";
		fahrkarten[3]="Kurzstrecke";
		fahrkarten[4]="Tageskarte Berlin AB";
		fahrkarten[5]="Tageskarte Berlin BC";
		fahrkarten[6]="Tageskarte Berlin ABC";
		fahrkarten[7]="Kleingruppen-Tageskarte Berlin AB";
		fahrkarten[8]="Kleingruppen-Tageskarte Berlin BC";
		fahrkarten[9]="Kleingruppen-Tageskarte Berlin ABC";
		
		System.out.print("Wählen Sie Ihre Fahrkarte für den Tarifbereich Berlin AB aus:\n\n");
		for (int i=1; i<11;i++) {
			
			System.out.println("("+i+")"+fahrkarten[i-1]+": "+preise[i-1]+" Euro"+"\n");
		
		} 
		System.out.println("Ihre Wahl:");
		
		
		
		int wahl=tastatur.nextInt();
		if(wahl>=1&&wahl<=10) {
		preis=preise[wahl-1];
		}else {
			
			System.out.println("Ihre Eingabe ist ungültig!");
			System.exit(0);
		}
		System.out.print("Anzahl der Tickets zwischen einem und zehn Tickets: ");
		byte ticketanzahl = tastatur.nextByte();
		// Datentyp Byte ist ausreichend, da es sehr unwahrscheinlich ist,
		// dass mehr als 127 Tickets gekauft werden

		if (ticketanzahl >= 1 && ticketanzahl <= 10) {

			// Vorgang
			gesamtbetrag = ticketanzahl * preis;
			return gesamtbetrag;
			
		} else {
				
			while (ticketanzahl < 1 || ticketanzahl>10) {
		
				//System.out.println("Ihre Eingabe ist ungültig, 
				//somit wird mit dem Standartwert 1 Ticket fortgefahren");
				//ticketanzahl=1;
				//gesamtbetrag=ticketanzahl *preis;
				//return gesamtbetrag; 
				
				System.out.println("Ihr Eingabewert ist ungültig. Geben Sie erneut einen Wert ein!\n");
				System.out.println("Ihre Wahl: ");
				byte ticketneu=tastatur.nextByte();
				ticketanzahl=ticketneu; 
			
				
				// Vorgang	
			}
		
			gesamtbetrag = ticketanzahl * preis;
			return gesamtbetrag;
		}

	}

	public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
	// Vorgang
	Scanner tastatur = new Scanner(System.in);
		double eingezahlterGesamtbetrag = 0.0;
	//Verzweigung
		while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {
			// Vorgang
			System.out.printf("%s %.2f %s \n", "Noch zu zahlen: ", (zuZahlenderBetrag - eingezahlterGesamtbetrag),
					"EURO");
			System.out.print("Eingabe (mind. 5Ct): ");
			
	//Eingaben
			double eingeworfeneMünze = tastatur.nextDouble();
	// Vorgang
	eingezahlterGesamtbetrag += eingeworfeneMünze;

		if(eingeworfeneMünze<=0.05) {
			System.out.println("Falsche Eingabe.");
			System.exit(0);
		}
		}
	// Vorgang
	return eingezahlterGesamtbetrag;
	}

	public static void fahrkartenAusgeben() {
		System.out.println("\nFahrschein wird ausgegeben");
		for (int i = 0; i < 8; i++) {
			System.out.print("=");
			warte(100);
			
		}	System.out.println("\n\n");

	}
	

	public static void warte(int millisekunde) {
		
			try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	
	public static void muenzeAusgeben(int betrag, String einheit) {
		System.out.println("       **      ");
		System.out.println("   *        *");
		System.out.println("  *   "+betrag+"     *  ");
		System.out.println("  *   "+einheit+"   *  ");
		System.out.println("   *        *");
		System.out.println("       **      \n");
		//System.out.println(betrag + " " + einheit);
	}

	public static void rueckgeldAusgeben(double eingezahlterGesamtbetrag, double zuZahlenderBetrag) {
		double rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;

			
	
		if (rückgabebetrag > 0.0) {
			System.out.printf("%s %.2f %s \n", "Der Rückgabebetrag in Höhe von ", rückgabebetrag, " EURO");
			System.out.println("wird in folgenden Münzen ausgezahlt:");

			while (rückgabebetrag >= 10.0) // 10 EURO-Schein
			{
				muenzeAusgeben(10, "EURO");
				rückgabebetrag -= 10.0;
			}
			while (rückgabebetrag >= 5.0) // 5 EURO-Schein
			{
				muenzeAusgeben(5, "EURO");
				rückgabebetrag -= 5.0;
			}
				while (rückgabebetrag >= 2.0) // 2 EURO-Münzen
			{
				muenzeAusgeben(2, "EURO");
				
				rückgabebetrag -= 2.0;
			}
			while (rückgabebetrag >= 1.0) // 1 EURO-Münzen
			{
				muenzeAusgeben(1, "EURO");
				rückgabebetrag -= 1.0;
			}
			while (rückgabebetrag >= 0.5) // 50 CENT-Münzen
			{
				muenzeAusgeben(50, "CENT");
				rückgabebetrag -= 0.5;
			}
			while (rückgabebetrag >= 0.2) // 20 CENT-Münzen
			{
				muenzeAusgeben(20, "CENT");
				rückgabebetrag -= 0.2;
			}
			while (rückgabebetrag >= 0.1) // 10 CENT-Münzen
			{
				muenzeAusgeben(10, "CENT");	
				rückgabebetrag -= 0.1;
			}
			while (rückgabebetrag >= 0.05)// 5 CENT-Münzen
			{
				muenzeAusgeben(5, "CENT");
				rückgabebetrag -= 0.05;
			}}
			
		
		}public static int abfrage() {
			Scanner tastatur= new Scanner(System.in);
			System.out.println("Für einen weiteren Vorgang drücken Sie die 1. Wenn sie ihren Kaufvorgang beendet haben, drücken Sie die 2!");
			int eingabe=tastatur.nextInt(); 
			return eingabe;
			}
		public static void verabschieden() {
		System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
				+ "Wir wünschen  Ihnen eine gute Fahrt.");
		}

	public static void main(String[] args) {
		
		for(int i=0;i>=0;i++) {

		double zuZahlenderBetrag = fahrkartenbestellungErfassen();
		double eingezahlterGesamtbetrag = fahrkartenBezahlen(zuZahlenderBetrag);
		fahrkartenAusgeben();
		
		double rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
		rueckgeldAusgeben(eingezahlterGesamtbetrag, zuZahlenderBetrag);
		
		int eingabe=abfrage();  
		if(eingabe==1) {
			System.out.println("Ein weiterer Kaufvorgang wird gestartet");
			i=0; 
		}else {
			i=-2;
		}
		
}
		verabschieden();
		}}
