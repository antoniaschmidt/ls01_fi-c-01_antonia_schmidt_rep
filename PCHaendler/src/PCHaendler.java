import java.util.Scanner;

public class PCHaendler {

	public static String liesString(String text) {
		Scanner myScanner = new Scanner(System.in);
		System.out.println(text);

		String artikel = myScanner.next();
		return artikel;

	}

	public static int liesInt(String text) {
		Scanner myScanner = new Scanner(System.in);

		System.out.println(text);
		int anzahl = myScanner.nextInt();
		return anzahl;

	}

	public static double liesDouble(String text) {
		Scanner myScanner = new Scanner(System.in);
		System.out.println(text);
		double preis = myScanner.nextDouble();
		System.out.println(text);
		double mwst = myScanner.nextDouble();
		return preis;

	}

	public static double berechneGesamtnettopreis(int anzahl, double preis) {
		double nettogesamtpreis = anzahl * preis;

		return nettogesamtpreis;

	}

	public static double berechneGesamtbruttopreis(double nettogesamtpreis, double mwst) {
		double bruttogesamtpreis = nettogesamtpreis * (1 + mwst / 100);
		return bruttogesamtpreis;

	}

	public static void rechnungausgeben(String artikel, int anzahl, double nettogesamtpreis, double bruttogesamtpreis,
			double mwst) {
		System.out.println("\tRechnung");
		System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", artikel, anzahl, nettogesamtpreis);
		System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", artikel, anzahl, bruttogesamtpreis, mwst, "%");
	}

	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);

		String artikel = liesString("Was möchten Sie bestellen?");
		int anzahl = liesInt("Geben Sie die Anzahl ein:");
		double preis = liesDouble("Geben Sie den Nettopreis ein:");
		double mwst = liesDouble("Geben Sie den Mehrwertsteuersatz in Prozent ein:");
		double nettogesamtpreis = berechneGesamtnettopreis(anzahl, preis);
		double bruttogesamtpreis = berechneGesamtbruttopreis(nettogesamtpreis, mwst);
		rechnungausgeben(artikel, anzahl, nettogesamtpreis, bruttogesamtpreis, mwst);

	}

}
