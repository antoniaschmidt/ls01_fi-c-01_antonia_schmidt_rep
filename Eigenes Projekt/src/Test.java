
public class Test {

	public static void main(String[] args) {
		
		// Zuweisung
		// int zahl3,zahl4;

		int zahl1 = 23; // Deklaration und Initialisierung
		int zahl2 = 75;

		// if Schlüsselwort zur Auswahl
		// Bedingung: wenn Zahl 1 kleiner als Zahl 2
		
		if (zahl1 < zahl2) {

			System.out.println("Zahl 1 ist kleiner als Zahl 2");

		} else if (zahl1 > zahl2) {

			System.out.println("Zahl 1 ist größer als Zahl 2.");
		}

		else if (zahl1 == zahl2) {

			System.out.println("Zahl 1 ist gleich Zahl 2");

		}

	}

}
